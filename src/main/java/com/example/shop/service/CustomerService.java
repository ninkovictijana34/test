package com.example.shop.service;

import com.example.shop.db.model.Customer;
import com.example.shop.model.CustomerDTO;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Optional;

public interface CustomerService {
   List<CustomerDTO> findAllCustomers();
   CustomerDTO findCostumerById();

}
