package com.example.shop.serviceimpl;

import com.example.shop.db.dao.CustomerRepository;
import com.example.shop.db.model.Customer;
import com.example.shop.model.CustomerDTO;
import com.example.shop.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CustomerServiceImpl implements CustomerService {
    @Autowired
    private CustomerRepository customerRepository;

    @Override
    public CustomerDTO findCostumerById() {
        Customer customer= customerRepository.findById(7).orElseThrow(EntityNotFoundException::new);

        CustomerDTO customerDTO=new CustomerDTO();

        customerDTO.setIdCustomer(customer.getId());
        customerDTO.setFirstName(customer.getFirstName());
        customerDTO.setLastName(customer.getLastName());

        return customerDTO;

    }

    @Override
    public List<CustomerDTO> findAllCustomers() {
        List<Customer> customers = customerRepository.findAll();
        List<CustomerDTO> customerDTOs = new ArrayList<>();

        for (Customer customer : customers) {
            CustomerDTO customerDTO = new CustomerDTO();
            customerDTO.setFirstName(customer.getFirstName());
            customerDTO.setLastName(customer.getLastName());
            customerDTO.setIdCustomer(customer.getId());

            customerDTOs.add(customerDTO);
        }

        return customerDTOs;
    }


}
