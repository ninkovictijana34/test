package com.example.shop.db.dao;
import com.example.shop.db.model.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

//public interface CustomerRepository {
//}
public interface CustomerRepository extends JpaRepository<Customer, Integer> {

    List<Customer> findByLastName(String lastName);

}
