package com.example.shop.controller;

import com.example.shop.model.CustomerDTO;
import com.example.shop.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@RestController
public class CustomerController {
    @Autowired
    private CustomerService customerService;

    @GetMapping("/customers")
    public List<CustomerDTO> findAlLCustomers() {
        return customerService.findAllCustomers();
    }

    @GetMapping("/idcustomer")
    public CustomerDTO findCustomerById() {
        return customerService.findCostumerById();

    }

    //@GetMapping("/test")
    //  ResponseEntity<String> studentR(@RequestParam("id") Integer id) {
    //     return ResponseEntity.ok("Your age is " + id);
    // }

    @RequestMapping(value = "/test", method = RequestMethod.POST)
    @ResponseBody
    public void test(@RequestParam(value = "i", defaultValue = "10") int i) {
    }

    @RequestMapping("/testiraj")
    public ModelAndView showWelcomePage(@RequestParam(value="fName", required=true) String firstname, @RequestParam(value="lName") String lastname) {

        String fullname = firstname + " " + lastname;
        System.out.println("Username is= " + fullname);

        ModelAndView m = new ModelAndView();
        m.addObject("fullname", fullname);
        m.setViewName("success");
        return m;


    }

}
